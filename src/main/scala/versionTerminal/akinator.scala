import scala.io.Source
import java.io._

trait ABanimal
case class Animal(nom:String) extends ABanimal
case class Question(q:String, oui:ABanimal, non:ABanimal) extends ABanimal



def nbNoeuds(ab : ABanimal):Int =  ab match {

	case Animal(_) => 1
	case Question(_,fg,fd) => nbNoeuds(fg) + nbNoeuds(fd) + 1
	
} 

def fichierToABanimal(nomf:String):ABanimal = {

	val l = Source.fromFile(nomf).getLines.toList

	
	def aux(l:List[String]):ABanimal = l match {

		case Nil => throw new Exception()
		case t::Nil => Animal(t)
		case h::t => if (h.startsWith("q:")) {
 
				val fg = aux(t) ; 
				Question(h, fg, aux(t.drop(nbNoeuds(fg))))

			     } else Animal(h) 

	}

	aux(l)
}



def ABanimalToFichier(nomf:String, a : ABanimal):Unit = {

	val writer = new FileWriter(new File(nomf));

	def aux(ab : ABanimal):String = ab match {

	case Animal(nom) => nom
	case Question(q, fg, fd) => {
				 	writer.write(q + "\n"); 
					writer.write(aux(fg) + "\n") ; 	
					writer.write(aux(fd)) ; 
					"" 
			       	    }
	}

	aux(a);
	writer.close();
}



def jeuSimple(a : ABanimal, it:Iterator[String]):Boolean = {


	def aux(ab : ABanimal):Boolean = ab match {

		case Animal(nom) => { 
					println("Pensez-vous à : '" + nom + "' ?") ; 

					if(it.next() == "o") { 

						println("J'ai gagné") ; 
						true 

					} else { 

						println("J'ai perdu") ; 
						false 
					}
				    }

		case Question(q, fg ,fd) => { 

						println(q) ; 

						if(it.next() == "o") {
	 
							aux(fg)

						 } else {

							aux(fd)
						} 
					   }
	}

	aux(a)
}




def jeuLog(a : ABanimal, it:Iterator[String]):List[String] = {

	

	def aux(ab : ABanimal):List[String] = ab match {

		case Animal(nom) => { 

					println("Pensez-vous à : '" + nom + "' ?") ; 

					if(it.next() == "o") { 

						 println("J'ai gagné") ;
						 List("o")

					 } else { 

						 println("J'ai perdu") ;
						 List("n") } 
				    }


		case Question(q, fg ,fd) => { 

						println(q) ; 
						val suivant = it.next();
	 
						if(suivant == "o") {

							 (suivant :: aux(fg)) 
						} else { 

							(suivant :: aux(fd)) 
						}
					   }
	}

	aux(a)
}



def jeuApprentissage(a : ABanimal, it:Iterator[String]):ABanimal = {

	

	def aux(ab : ABanimal):ABanimal = ab match {

		case Animal(nom) => { 
					println("Pensez-vous à : '" + nom + "' ?") ;
			
					 if(it.next() == "o") {
		
						 println("J'ai gagné") ; 
						 Animal(nom) 

					} else {

			 			 println("J'ai perdu - quelle est la bonne réponse ?") ; 
						 val newAnim = it.next();

						 println("Quelle question permet de différencier " + newAnim + " de " + nom + " ?");
						 val newQuestion = "q:" + it.next();

						 println("Quelle est la réponse à cette question pour " + newAnim + " ?");
						 val reponseQuestion = it.next();
						
						if (reponseQuestion == "o") {

							 new Question(newQuestion,new Animal(newAnim), Animal(nom))

 
						} else { 
							
							 new Question(newQuestion,new Animal(nom), Animal(newAnim))
						}	
				        }
				   }



		case Question(q, fg ,fd) => { 

						println(q) ;
					
						if(it.next() == "o") {

							new Question(q,aux(fg),fd)

						} else {

							 new Question(q,fg,aux(fd)) 
						}
					    }
	}

	aux(a)
}





def jeuSimpleJNSP(a : ABanimal, it:Iterator[String]):Boolean = {

	

	def aux(ab : ABanimal, lRestants: List[ABanimal]):Boolean = ab match {

		case Animal(nom) => { 
					println("Pensez-vous à : '" + nom + "' ?") ; 

					if(it.next() == "o") { 

						println("J'ai gagné") ; 
						true

					 } else { 

						if(lRestants.isEmpty) {

							 println("J'ai perdu") ;
							 false

						 } else {

							aux(lRestants.head, lRestants.drop(1)) 
						 }
					 }
				    } 


		case Question(q, fg ,fd) => { 

						println(q) ;
						val charCour = it.next() ; 

						if( charCour == "o") {

							 aux(fg,lRestants)

						} else { 

							if(charCour == "x") {

								aux(fg, fd::lRestants)

							} else {

								 aux(fd, lRestants)
							}
					      }
					  }

	}

	aux(a, List())
}


def lancerJeuSansMenu():Unit =  {
  
  
  	val arbreCharge = fichierToABanimal("donnees.txt")
  
  	ABanimalToFichier("donnees.txt" ,jeuApprentissage(arbreCharge, Source.stdin.getLines))
  
  	println("Voulez-vous rejouer ?")
  	
  	val choix = Source.stdin.getLines.next()
  
  	if(choix == "o"){
  
  	  println("")
  	  lancerJeuSansMenu()
  
  	}
  
  }


  
  def lancerJeuAvecMenu():Unit = {
    
    val arbreCharge = fichierToABanimal("donnees.txt")
    val iterateur = Source.stdin.getLines
    
    println("Quel mode de jeu voulez-vous ?")
    println("\t(1) - Jeu Simple")
    println("\t(2) - Jeu Log")
    println("\t(3) - Jeu Apprentissage")
    println("\t(4) - Jeu Simple - Je Ne Sais Pas (JNSP)")
    println("Entrez le nombre correspondant au mode souhaité.")
    
    val mode = iterateur.next()
    
    println("________________________________________________")
    println("")
    
    mode match {
      case "1" => println("Jeu Simple") ; jeuSimple(arbreCharge, iterateur)
      case "2" => println("Jeu Log") ; println(jeuLog(arbreCharge, iterateur))
      case "3" => println("Jeu Apprentissage") ; ABanimalToFichier("donnees.txt" ,jeuApprentissage(arbreCharge, Source.stdin.getLines))
      case "4" => println("Jeu Simple - Je Ne Sais Pas (JNSP)") ; jeuSimpleJNSP(arbreCharge, iterateur)
      case _ => println("Veuillez entrer un nombre valide !") ; lancerJeuAvecMenu()
    }
    
    println("")
    println("Voulez-vous rejouer ?")
    val choix = iterateur.next()
  
  	if(choix == "o"){
  
  	  println("")
  		lancerJeuAvecMenu()
  
  	}
    
  }








